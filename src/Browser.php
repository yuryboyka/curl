<?php

namespace YuryBoyka\Curl;

class Browser
{
    /**
     * @var CookieJar
     */
    private $cookie;

    /**
     * @var string
     */
    private $proxy = '';

    /**
     * @var string
     */
    private $proxyUserPwd = '';

    /**
     * @var string
     */
    private $userAgent = '';

    /**
     * @var int
     */
    private $timeout = 25;

    /**
     * @var array
     */
    public $headers = [];

    /**
     * @var bool
     */
    private $followLocation = true;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string|array
     */
    private $requestParams;

    /**
     * @var string
     */
    private $method = 'get';

    /**
     * @var float
     */
    private $requestDelay = 0;

    /**
     * @var string
     */
    private static $userAgentFile = __DIR__  . '/../user-agents.txt';

    /**
     * @param CookieJar|null $cookieJar
     */
    public function __construct(CookieJar $cookieJar = null)
    {
        if (is_null($cookieJar)) {
            $cookieJar = new CookieJar();
        }

        $this->cookie = $cookieJar;
    }

    /**
     * @param bool $flag
     * @return Browser
     */
    public function setFollowLocation(bool $flag): Browser
    {
        $this->followLocation = $flag;
        return $this;
    }

    /**
     * @param string $proxy
     * @param string $proxyUserPwd
     * @return Browser
     */
    public function setProxy(string $proxy, string $proxyUserPwd = ''): Browser
    {
        $this->proxy = $proxy;
        $this->proxyUserPwd = $proxyUserPwd;
        return $this;
    }

    /**
     * @param string $cookie
     * @return Browser
     */
    public function setCookie(string $cookie): Browser
    {
        $this->cookie->set($cookie);
        return $this;
    }

    /**
     * @param string $userAgent
     * @return Browser
     */
    public function setUserAgent(string $userAgent): Browser
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return Browser
     * @throws CurlException
     */
    public function setRandomUserAgent(): Browser
    {
        $this->setUserAgent(self::getRandomUserAgent());
        return $this;
    }

    /**
     * @return string
     * @throws CurlException
     */
    public static function getRandomUserAgent(): string
    {
        $userAgentFile = realpath(self::$userAgentFile);

        if (!file_exists($userAgentFile)) {
            throw new CurlException(CurlException::USER_AGENT_FILE_NOT_EXISTS);
        }

        if (!$fileContent = trim(file_get_contents($userAgentFile))) {
            throw new CurlException(CurlException::EMPTY_USER_AGENT_FILE);
        }

        $rows = preg_split('/[\r]?[\n]/', $fileContent);
        $rowCnt = count($rows);
        $randIdx = rand(0, $rowCnt - 1);

        return $rows[$randIdx];
    }

    /**
     * @param int $timeout
     * @return Browser
     * @throws CurlException
     */
    public function setTimeout(int $timeout): Browser
    {
        if ($timeout < 1) {
            throw new CurlException(CurlException::INVALID_TIMEOUT);
        }

        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @param array $headers
     * @return Browser
     */
    public function setHeaders(array $headers): Browser
    {
        foreach ($headers as $key => $value) {
            if (is_null($value)) {
                unset($this->headers[$key]);
            } else {
                $this->headers[$key] = $value;
            }
        }

        return $this;
    }

    public function setRequestDelay(float $delay): Browser
    {
        $this->requestDelay = $delay;
        return $this;
    }

    /**
     * @return CookieJar
     */
    public function cookie(): CookieJar
    {
        return $this->cookie;
    }

    /**
     * @return Browser
     */
    public function request(): Browser
    {
        return clone $this;
    }

    /**
     * @param string $url
     * @return Response
     * @throws CurlException
     */
    public function get(string $url): Response
    {
        $this->url = $url;
        $this->method = 'get';
        return $this->execute();
    }

    /**
     * @param string $url
     * @param string|array $params
     * @return Response
     * @throws CurlException
     */
    public function post(string $url, $params = []): Response
    {
        $this->url = $url;
        $this->method = 'post';
        $this->requestParams = $params;
        return $this->execute();
    }

    /**
     * @param string $url
     * @param array $params
     * @return Response
     * @throws CurlException
     */
    public function json(string $url, array $params = []): Response
    {
        $this->url = $url;
        $this->method = 'post';
        $this->requestParams = json_encode($params, JSON_UNESCAPED_UNICODE);
        $this->headers['Content-Type'] = 'application/json; charset=UTF-8';
        return $this->execute();
    }

    /**
     * @return Response
     * @throws CurlException
     */
    private function execute(): Response
    {
        curl_setopt_array($ch = curl_init(), $this->getRequestParams());
        usleep($this->requestDelay * 1000000);
        $httpResponse = curl_exec($ch);

        if ($error = curl_error($ch)) {
            throw new CurlException($error);
        }

        $response = new Response($httpResponse, $ch);
        $cookie = $response->getHeader('Set-Cookie');
        $this->cookie->set($cookie, $this->url);

        if ($this->followLocation && $response->hasHeader('Location')) {
            $locationHeader = $response->getHeader('Location', 0);
            $location = $this->getLocation($locationHeader);
            $response = $this->get($location);
            RequestCounter::increment(true);
        } else {
            RequestCounter::increment();
        }

        return $response;
    }

    /**
     * @return array
     * @throws CurlException
     */
    private function getRequestParams(): array
    {
        if (empty($this->url)) {
            throw new CurlException(CurlException::NO_URL);
        }

        return [
                CURLOPT_URL            => $this->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER         => true,
                CURLOPT_TIMEOUT        => $this->timeout,
                CURLOPT_CONNECTTIMEOUT => $this->timeout
            ]
            + $this->getMethodParams()
            + $this->getHeaders()
            + $this->getProxyHeaders()
            + $this->getCookieParam();
    }

    /**
     * @return array
     */
    private function getMethodParams(): array
    {
        $returnData = [];

        if ('post' === $this->method) {
            $returnData = [CURLOPT_POST => true];

            if (!empty($this->requestParams)) {
                $returnData[CURLOPT_POSTFIELDS] = $this->requestParams;
                unset($this->requestParams);
            }
        }

        return $returnData;
    }

    /**
     * @return array
     */
    private function getHeaders(): array
    {
        if (empty($this->headers)) {
            return [];
        }

        $headers = [];

        foreach ($this->headers as $name => $value) {
            $headers[] = "{$name}: {$value}";
        }

        return [CURLOPT_HTTPHEADER => $headers];
    }

    /**
     * @return array
     */
    private function getProxyHeaders(): array
    {
        $headers = [];

        if (!empty($this->proxy)) {
            $headers[CURLOPT_PROXY] = $this->proxy;
            $headers[CURLOPT_HTTPPROXYTUNNEL] = true;
        }

        if (!empty($this->proxyUserPwd)) {
            $headers[CURLOPT_PROXYUSERPWD] = $this->proxyUserPwd;
        }

        return $headers;
    }

    /**
     * @return array
     */
    private function getCookieParam(): array
    {
        $cookie = $this->cookie->get($this->url);
        return empty($cookie) ? [] : [CURLOPT_COOKIE => $cookie];
    }

    /**
     * @param string $locationHeader
     * @return string
     */
    private function getLocation(string $locationHeader): string
    {
        $parts = parse_url($locationHeader);
        $currentParts = parse_url($this->url);
        $url = $parts + [
            'scheme' => $currentParts['scheme'] ?? '',
            'host' => $currentParts['host'] ?? ''
        ];

        return (empty($url['scheme']) ? '' : $url['scheme'] . '://')
            . (empty($url['user']) || empty($url['pass']) ? '' : "{$url['user']}@{$url['pass']}")
            . (empty($url['host']) ? '' : $url['host'])
            . (empty($url['port']) ? '' : ":{$url['port']}")
            . (empty($url['path']) ? '' : $url['path'])
            . (empty($url['query']) ? '' : "?{$url['query']}")
            . (empty($url['fragment']) ? '' : "#{$url['fragment']}");
    }
}
