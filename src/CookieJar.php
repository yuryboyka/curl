<?php

namespace YuryBoyka\Curl;

class CookieJar
{
    /**
     * @var array
     */
    private $cookie = [];

    /**
     * @param array|string|null $cookie
     */
    public function __construct($cookie = null)
    {
        $this->set($cookie);
    }

    /**
     * @param string|array $cookie
     * @param string $url
     */
    public function set($cookie, string $url = ''): void
    {
        if (empty($cookie)) {
            return;
        }

        $now = time();

        foreach ((array)$cookie as $item) {
            $parts = explode(';', trim($item));
            $pairs = $this->getPairs($parts);
            list($key, $value) = array_shift($pairs);
            unset($this->cookie[$key]);
            $cookieParams = [
                'value' => $value,
                'regex' => [
                    'secure' => false,
                    'host' => $this->getDomainFromHost(parse_url($url, PHP_URL_HOST)),
                    'path' => ''
                ]
            ];

            foreach ($pairs as $pair) {
                list($pairKey, $pairValue) = $pair;

                switch (strtolower($pairKey)) {
                    case 'secure':
                        $cookieParams['regex']['secure'] = true;
                        break;
                    case 'max-age':
                        $cookieParams['expires'] = $now + $pairValue;
                        break;
                    case 'expires':
                        if (!array_key_exists('expires', $cookieParams)) {
                            $cookieParams['expires'] = $now + strtotime($pairValue);
                        }

                        break;
                    case 'domain':
                        $cookieParams['regex']['host'] = $this->getDomainFromHost($pairValue);
                        break;
                    case 'path':
                        $cookieParams['regex']['path'] = trim($pairValue, '/');
                        break;
                }
            }

            if (!array_key_exists('expires', $cookieParams) || $cookieParams['expires'] > $now) {
                $this->cookie[$key] = $cookieParams;
            }
        }
    }

    /**
     * @param array $cookieParts
     * @return array
     */
    private function getPairs(array $cookieParts): array
    {
        $pairs = [];

        foreach ($cookieParts as $part) {
            $pairs[] = $this->getKeyValue($part);
        }

        return $pairs;
    }

    /**
     * @param string $keyValueString
     * @return array
     */
    private function getKeyValue(string $keyValueString): array
    {
        $parts = explode('=', $keyValueString);
        return [$parts[0], $parts[1] ?? ''];
    }

    /**
     * @param string $host
     * @return string
     */
    private function getDomainFromHost(string $host): string
    {
        $parts = array_reverse(explode('.', $host));
        return (count($parts) > 1) ? "{$parts[1]}.{$parts[0]}" : $host;
    }

    /**
     * @param string $url
     * @return string
     */
    public function get(string $url = ''): string
    {
        $cookie = [];
        $now = time();

        foreach ($this->cookie as $key => $params) {
            if (array_key_exists('expires', $params) && $params['expires'] <= $now) {
                unset($this->cookie[$key]);
            } elseif (empty($url) || empty($params['regex']) || $this->comparedUrl($url, $params['regex'])) {
                $cookie[] = "{$key}={$params['value']}";
            }
        }

        return implode('; ', $cookie);
    }

    /**
     * @param string $url
     * @param array $regex
     * @return bool
     */
    private function comparedUrl(string $url, array $regex): bool
    {
        $parts = parse_url($url);

        return $this->compareScheme($parts, $regex)
            && $this->compareDomain($parts, $regex)
            && $this->comparePath($parts, $regex);
    }

    /**
     * @param array $parts
     * @param array $regex
     * @return bool
     */
    private function compareScheme(array $parts, array $regex): bool
    {
        return (empty($parts['scheme']) || empty($regex['secure']) || 'https' === $parts['scheme']);
    }

    /**
     * @param array $parts
     * @param array $regex
     * @return bool
     */
    private function compareDomain(array $parts, array $regex): bool
    {
        return (empty($parts['host']) || empty($regex['host'])
            || $this->getDomainFromHost($parts['host']) === $regex['host']);
    }

    /**
     * @param array $parts
     * @param array $regex
     * @return bool
     */
    private function comparePath(array $parts, array $regex): bool
    {
        return (empty($parts['path']) || empty($regex['path'])
            || preg_match('/^' . preg_quote($regex['path']) . '/i', $parts['path']));
    }

    /**
     * @param string $name
     * @param mixed|null $default
     * @return string
     */
    public function getByName(string $name, $default = null): string
    {
        return array_key_exists($name, $this->cookie) ? $this->cookie[$name]['value'] : $default;
    }
}
