<?php

namespace YuryBoyka\Curl;

use Exception;

class CurlException extends Exception
{
    const
        INVALID_TIMEOUT               = 'Timeout не может быть меньше 1 секунды',
        NO_URL                        = 'Не указан URL',
        HOST_NOT_FOUND_IN_REQUEST_URL = 'В URL запроса не найден хост',
        USER_AGENT_FILE_NOT_EXISTS    = 'Не найден файл со списком User-Agent`ов',
        EMPTY_USER_AGENT_FILE         = 'Файл со списком User-Agent`ов пуст',
        CANT_RECEIVE_PAGE             = 'Не удалось получить страницу %s. Код %d';
}
