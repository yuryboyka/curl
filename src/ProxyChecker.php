<?php

namespace YuryBoyka\Curl;

class ProxyChecker
{
    /**
     * @var string
     */
    private $url = 'http://icanhazip.com/';

    /**
     * @var float
     */
    private $timeout = 5;

    /**
     * @var Browser
     */
    private $browser;

    /**
     * @throws CurlException
     */
    public function __construct()
    {
        $this->browser = new Browser();
        $this->browser->setTimeout($this->timeout);
    }

    /**
     * @param string $url
     * @return ProxyChecker
     */
    public function setUrl(string $url): ProxyChecker
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param float $timeout
     * @return ProxyChecker
     * @throws CurlException
     */
    public function setTimeout(float $timeout): ProxyChecker
    {
        $this->browser->setTimeout($timeout);
        return $this;
    }

    /**
     * @param array $proxies
     * @param bool $fullInfo
     * @return array
     */
    public function checkList(array $proxies, bool $fullInfo = false): array
    {
        $valid = [];

        foreach ($proxies as $proxy) {
            $result = $this->checkProxy($proxy, $fullInfo);

            if ($fullInfo) {
                $result['proxy'] = $proxy;
                $valid[] = $result;
            } elseif ($result) {
                $valid[] = $proxy;
            }
        }

        return $valid;
    }

    /**
     * @param string $proxy
     * @param bool $fullInfo
     * @return bool|array
     */
    public function checkProxy(string $proxy, bool $fullInfo = false)
    {
        $parts = explode(',', $proxy);

        if (!preg_match('/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{2,5})/', $proxy, $matches)) {
            return ($fullInfo) ? ['status' => false, 'error' => 'Invalid proxy format'] : false;
        }

        try {
            $response = $this->browser->request()
                ->setProxy($matches[1], $parts[1] ?? '')
                ->get($this->url);
            $valid = (200 === $response->statusCode());

            if ($fullInfo) {
                if ($valid) {
                    $info = $response->requestInfo();
                    return ['status' => true, 'timeout' => $info['total_time']];
                }

                return ['status' => false, 'error' => "Invalid status: {$response->statusCode()}"];
            }

            return $valid;
        } catch (CurlException $e) {
            return ($fullInfo) ? ['status' => false, 'error' => $e->getMessage()] : false;
        }
    }
}
