<?php

namespace YuryBoyka\Curl;

class RequestCounter
{
    /**
     * @var int
     */
    private static $requests = 0;

    /**
     * @var int
     */
    private static $followRequests = 0;

    /**
     * @param bool $follow
     */
    public static function increment(bool $follow = false): void
    {
        self::$requests++;

        if ($follow) {
            self::$followRequests++;
        }
    }

    /**
     * @param bool $withFollow
     * @return int
     */
    public static function get(bool $withFollow = true): int
    {
        return ($withFollow) ? self::$requests : self::$requests - self::$followRequests;
    }

    public static function reset(): void
    {
        self::$requests = 0;
        self::$followRequests = 0;
    }
}
