<?php

namespace YuryBoyka\Curl;

class Response
{
    /**
     * @var mixed
     */
    private $ch;

    /**
     * @var array
     */
    private $sets = [];

    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var string
     */
    private $statusText;

    /**
     * @var string
     */
    private $body;

    /**
     * @param string $httpResponse
     * @param $ch
     */
    public function __construct(string $httpResponse, $ch)
    {
        $this->ch = $ch;
        $responseParts = explode("\r\n\r\n", $httpResponse);
        $partsCnt = count($responseParts);

        for ($i = 0; $i < $partsCnt; $i++) {
            $part = trim($responseParts[$i]);

            if (preg_match('/^HTTP\/\d\.\d/', $part)) {
                $this->parseHeaders($part);
            } else if ('' !== $part || $i = $partsCnt - 1) {
                $lastSet = end($this->sets);
                $this->statusCode = $lastSet['status']['code'];
                $this->statusText = $lastSet['status']['text'];
                $this->headers = $lastSet['headers'];
                $this->body = implode("\r\n\r\n", array_slice($responseParts, $i));

                if ('gzip' === $this->getHeader('Content-Encoding', 0)) {
                    $this->body = gzdecode($this->body);
                }

                break;
            }
        }
    }

    /**
     * @param string $httpHeaderString
     */
    private function parseHeaders(string $httpHeaderString): void
    {
        $rows = preg_split('/[\r]?[\n]/', trim($httpHeaderString));
        $set = [];

        foreach ($rows as $header) {
            if (preg_match('/^HTTP\/\d\.\d (\d{3}) ?(.+)?$/', $header, $matches)) {
                $set['status'] = [
                    'code' => (int)$matches[1],
                    'text' => $matches[2] ?? ''
                ];
            } elseif (preg_match('/^(.+?): (.+?)$/', $header, $matches)) {
                $headerName = $this->normalizeHeaderName($matches[1]);

                if (empty($set['headers'][$headerName])) {
                    $set['headers'][$headerName] = [];
                }

                $set['headers'][$headerName][] = $matches[2];
            }
        }

        $this->sets[] = $set;
    }

    /**
     * @param string $headerName
     * @return string
     */
    public function normalizeHeaderName(string $headerName): string
    {
        $parts = preg_split('/[-_]/', $headerName);

        foreach ($parts as $key => $part) {
            $parts[$key] = ucfirst(strtolower($part));
        }

        return implode('-', $parts);
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return (string)$this->body;
    }

    /**
     * @param string $headerName
     * @return bool
     */
    public function hasHeader(string $headerName): bool
    {
        $headerName = $this->normalizeHeaderName($headerName);
        return !empty($this->headers[$headerName]);
    }

    /**
     * @param string $headerName
     * @param int $idx
     * @return array|string
     */
    public function getHeader(string $headerName, int $idx = -1)
    {
        $headerName = $this->normalizeHeaderName($headerName);
        $header = $this->headers[$headerName] ?? [];

        if (-1 === $idx) {
            return $header;
        }

        return (is_array($header) && array_key_exists($idx, $header)) ? $header[$idx] : '';
    }

    /**
     * @param bool $all
     * @return array
     */
    public function getHeaders(bool $all = false): array
    {
        return ($all) ? $this->sets : $this->headers;
    }

    /**
     * @return int
     */
    public function statusCode(): int
    {
        return (int)$this->statusCode;
    }

    /**
     * @return string
     */
    public function statusText(): string
    {
        return $this->statusText;
    }

    /**
     * @return array
     */
    public function requestInfo(): array
    {
        return curl_getinfo($this->ch);
    }
}
